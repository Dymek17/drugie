CC=gcc
CFLAGS=-Wall
LIBS=-lm 

main2: main2.o srednia2.o mediana2.o odchylenie2.o
	$(CC) $(CFLAGS) -o main2 main2.o srednia2.o mediana2.o odchylenie2.o $(LIBS)

main2.o: main2.c funkcje2.h
	$(CC) $(CFLAGS) -c main2.c

odchylenie2.o: odchylenie2.c
	$(CC) $(CFLAGS) -c odchylenie2.c

mediana2.o: mediana2.c
	$(CC) $(CFLAGS) -c mediana2.c

srednia2.o: srednia2.c
	$(CC) $(CFLAGS) -c srednia2.c